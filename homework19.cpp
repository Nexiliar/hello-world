// homework19.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>


class Animal
{
public:
    virtual void voice()
    {
        std::cout<< "AnimalSound\n";
    }
    virtual ~Animal() {};   
};
class Dog:public Animal
{
public:
    void voice() override
    {
        std::cout << "Woof\n";
    }
};

class Cat :public Animal
{
public:
    void voice() override
    {
        std::cout << "Mew\n";
    }
};

class DrunkHobo :public Animal
{
public:
    void voice() override
    {
        std::cout << "Mmmm mrrrggk\n";
    }
};

int main()
{

   Animal* array[3] = { new DrunkHobo(), new Dog(),new Cat() };
     
   for (int i = 0; i < 3; i++)
   {
       array[i]->voice();
       delete array[i];
   }
 
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file

/*������� ����� Animal � ��������� ������� Voice() ������� ������� � ������� ������ � �������.
����������� �� Animal ������� ��� ������(� ������� Dog, Cat � �.�.) � � ��� ����������� ����� Voice() ����� �������, ����� ��� ������� � ������ Dog ����� Voice() ������� � ������� "Woof!".
� ������� main ������� ������ ���������� ���� Animal � ��������� ���� ������ ��������� ��������� �������.
����� �������� ������ �� �������, ������� �� ������ �������� ������� ����� Voice().
�������������� ��� ������, ������ ���������� ��������� �� ����� ������� ����������� Animal.*/